import mongoose from "mongoose";

var Schema = mongoose.Schema
const repoSchema = new Schema(
    {
        id: { type: String, unique: true },
        name: { type: String },
        image: { type: String },
        repo: { type: String },
    }
);


var repo = mongoose.model('repo', repoSchema)

export default repo;
