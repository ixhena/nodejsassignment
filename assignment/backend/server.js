import dotenv from 'dotenv';
import Axios from "axios";
import express from "express";
import path from 'path';
import mongoose from "mongoose";
import repo from './repo.js';

const app = express();
dotenv.config();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect(process.env.MOGODB_URL || "mongodb://localhost/github", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
});

var access_token = "";

app.get("/", (req, res) => {
    res.send("Server is ready");
});
const __dirname = path.resolve();
app.use(express.static(path.join(__dirname, '/frontend/build')));
const clientID = 'd49e9798d1294e49bfbd'
const clientSecret = '6eed230f4bed0652ddd539a455bc1a99fa782c3b'

app.get('/github/callback', (req, res) => {
    const requestToken = req.query.code
    Axios({
        method: 'post',
        url: `https://github.com/login/oauth/access_token?client_id=${clientID}&client_secret=${clientSecret}&code=${requestToken}`,
        headers: {
            accept: 'application/json'
        }
    }).then((response) => {
        access_token = response.data.access_token
        res.redirect("http://localhost:3000/profile")
    })
})

app.post('/profile', function (req, res) {
    Axios({
        method: 'get',
        url: `https://api.github.com/user`,
        headers: {
            Authorization: 'token ' + access_token
        },
    }).then((response) => {
        var data = [
            {
                'id': response.data.id,
                'name': response.data.login,
                'image': response.data.avatar_url,
                'repo': response.data.repos_url
            },
        ]
        repo.collection.insertMany(data)
        res.send({ userData: response.data })
    })
});

app.use((err, req, res, next) => {
    res.status(500).send({ message: err.message });
});
const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server at http://localhost:${port}`);
});