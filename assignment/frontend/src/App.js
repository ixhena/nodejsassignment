import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import Login from "./pages/Login";
import Profile from "./pages/Profile";


function App() {
  return (
    <Router>
      <Switch>
        <Route
          exact
          path={`${process.env.PUBLIC_URL + "/"}`}
          component={Login}
        />
        <Route
          exact
          path={`${process.env.PUBLIC_URL + "/profile"}`}
          component={Profile}
        />
      </Switch>
    </Router>
  );
}

export default App;

