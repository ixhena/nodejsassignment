import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import ResultsStarredRepo from "./ResultsStarredRepo";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

function StarredRepos(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [data, setData] = useState([]);
    const [showResults, setShowResults] = React.useState(false)

    let reposUrl;
    if (props.data.length !== 0) {
        reposUrl = props.data.userData.repos_url;
    }
    useEffect(() => {
        fetch(reposUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setData(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])
    const showRepo = () => {
        setShowResults(true);
        if (document.getElementById("repostar") !== null) {
            var elms = document.querySelectorAll("[id='repostar']");
            for (var i = 0; i < elms.length; i++)
                elms[i].style.display = 'block';
        }
        if (document.getElementById("repos") !== null) {
            document.getElementById("repos").style.display = "none"
        }
    }
    var count = 0;
    if (data.length !== 0) {
        data.map((key) => {
            count += key.stargazers_count
        })
    }

    return (
        <>
            <FontAwesomeIcon
                icon={faStar}
            ></FontAwesomeIcon>
            <Link onClick={showRepo}> Stars</Link>
            <div class="counterRepoStarred">{count}</div>
            <>{showResults ? <ResultsStarredRepo props={data}></ResultsStarredRepo> : null}</>
        </>
    );
}

export default StarredRepos;
