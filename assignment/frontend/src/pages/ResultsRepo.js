import React, { useEffect, useState } from "react";

function ResultsRepo(props) {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [data, setData] = useState([]);

    let reposUrl;
    if (props.data.length !== 0) {
        reposUrl = props.data.userData.repos_url
    }
    useEffect(() => {
        fetch(reposUrl)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setData(result);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [])
    return (
        <div id="repos"  >
            {Object.keys(data).map((key) => {
                return (
                    <div className="repoNameRep">
                        <a href={data[key].clone_url} style={{ color: "#0968DA" }}> {data[key].name}</a>
                    </div>
                )
            })}
        </div>
    );
}

export default ResultsRepo;
