import React from "react";
import img from '../logo/Octocat.png'

function Login() {

    return (
        <div>
            <div class="container">
                <div class="authorization">
                    <img src={img} alt="" className="logoGit"></img>
                    <p ><span class="fa fa-github"></span> Github OAuth</p>
                    <p>Authorize your app with:</p>
                    <a href={`https://github.com/login/oauth/authorize?client_id=d49e9798d1294e49bfbd`} className="login"> Github Login</a>
                </div>
            </div>
        </div>
    );
}

export default Login;
