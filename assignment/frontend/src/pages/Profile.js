import axios from "axios";
import React, { useEffect, useState } from "react";
import StarredRepos from "./StarredRepos";
import { Container, Navbar } from 'react-bootstrap';
import { Link } from "react-router-dom";
import ResultsRepo from "./ResultsRepo";
import img from '../logo/Octocat.png'

function Profile() {
    const [data, setData] = useState([]);
    const [showResultsRepo, setShowResultsRepo] = React.useState(false);
    useEffect(() => {
        const fetchData = async () => {
            const { data } = await axios.post('/profile')
            setData(data);
        };
        fetchData()
    }, []);

    const showRepos = () => {
        setShowResultsRepo(true);
        if (document.getElementById("repostar") !== null) {
            var elms = document.querySelectorAll("[id='repostar']");
            for (var i = 0; i < elms.length; i++)
                elms[i].style.display = 'none';
        }
        if (document.getElementById("repos") !== null) {
            document.getElementById("repos").style.display = "block"
        }
    }

    return (
        <>
            <Navbar bg="dark">
                <Container>
                    <img src={img} alt="" className="logo"></img>
                    <img src="https://github.githubassets.com/images/modules/open_graph/github-logo.png" alt="" className="githublogo"></img>
                </Container>
            </Navbar>
            <Container className="header">
                <div className="repositories">
                    <Link onClick={showRepos} style={{ marginLeft: "-6px" }}> Repositories</Link>
                    <div class="counterRepo">{data.userData !== undefined ? data.userData.public_repos : ""}</div>
                    {showResultsRepo ? <ResultsRepo data={data} ></ResultsRepo> : null}
                </div>
                <div className="starred" id="starred">
                    {data.length !== 0 ?
                        <StarredRepos data={data}></StarredRepos > : <></>}
                </div>
            </Container>
            <Container className="info">
                {data.length !== 0 ?
                    <img className="image" src={data.userData.avatar_url} /> : <></>}
                {data.length !== 0 ? <p className="name">{data.userData.login}</p> : <></>}
            </Container>
        </>
    );
}

export default Profile;
