import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

function ResultsStarredRepo(props) {
    return (
        <>
            {Object.keys(props.props).map((key) => {
                if (props.props[key].stargazers_count == 1) {
                    return (
                        <div id="repostar" >
                            <div className="repoName" id="repoName">
                                <a href={props.props[key].clone_url} style={{ color: "#0968DA" }}> {props.props[key].name}</a>
                            </div>
                            <div className="starIcon" id="starIcon" >
                                <FontAwesomeIcon
                                    icon={faStar}
                                ></FontAwesomeIcon>
                                <span>Starred</span>
                            </div>
                        </div>
                    )
                } else {
                    return;
                }
            })}
        </>
    );
}

export default ResultsStarredRepo;
